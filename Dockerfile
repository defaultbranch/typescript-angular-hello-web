# inspired by <https://lipanski.com/posts/smallest-docker-image-static-website>
FROM docker.io/library/alpine:3.16.0
RUN apk add thttpd
RUN adduser -D static
USER static
WORKDIR /home/static
COPY dist/typescript-angular-hello-web .
CMD ["thttpd", "-D", "-h", "0.0.0.0", "-p", "3000", "-d", "/home/static", "-u", "static", "-l", "-", "-M", "60"]
