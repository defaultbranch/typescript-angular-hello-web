# TypeScript Angular Hello Web

A simple Angular Hello-World for kubernetes deployment
(using node.js/npm, Angular, Kubernetes).

## Goals and Funtionality

- run tests
- bundle the app
- create a container that serves the app as static content

## Specific Reading

There is a blog post on how to serve static content with a small container,
<https://lipanski.com/posts/smallest-docker-image-static-website>,
which was used to write the initial `Dockerfile`.

## Replay

The following assumes that `node`, `npm` and `ng` are installed.

### Initial Project Setup

- create repo <https://gitlab.com/defaultbranch/typescript-angular-hello-web>
- bootstrap via `ng new typescript-angular-hello-web` (see <https://angular.io/tutorial>)
- add or overwrite some project files (README.md, LICENSE, …) as convenient

### Initial Source Files

- the command `ng new …` already created source files
- change [src/app/app.component.html](src/app/app.component.html) to display a simple hello message
